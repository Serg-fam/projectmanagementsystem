package ua.goit.javaee.model;

import java.util.Set;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
public class Developer extends Person {

  private int age;
  private int salary;
  private Set<Skill> skills;

  public Developer(int id, String firstName, String lastName, int age, int salary) {
    super(id, firstName, lastName);
    this.age = age;
    this.salary = salary;
  }

  public Developer(int id, String firstName, String lastName, int age, int salary, Set<Skill> skills) {
    super(id, firstName, lastName);
    this.age = age;
    this.salary = salary;
    this.skills = skills;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public int getSalary() {
    return salary;
  }

  public void setSalary(int salary) {
    this.salary = salary;
  }

  public Set<Skill> getSkills() {
    return skills;
  }

  public void setSkills(Set<Skill> skills) {
    this.skills = skills;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Developer)) return false;

    Developer developer = (Developer) o;

    if (getId().equals(developer.getId())) return false;
    if (!getFirstName().equals(developer.getFirstName())) return false;
    if (!getLastName().equals(developer.getLastName())) return false;
    if (age != developer.age) return false;
    if (salary != developer.salary) return false;
    return skills.equals(developer.skills);
  }

  @Override
  public int hashCode() {
    int result = getId();
    result = 31 * result + getFirstName().hashCode();
    result = 31 * result + getLastName().hashCode();
    result = 31 * result + age;
    result = 31 * result + salary;
    result = 31 * result + skills.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Developer{" +
        "id=" + getId() +
        ", firstName='" + getFirstName() + '\'' +
        ", lastName='" + getLastName() + '\'' +
        ", age=" + age +
        ", salary=" + salary +
        ", skills=" + skills +
        '}';
  }

}
