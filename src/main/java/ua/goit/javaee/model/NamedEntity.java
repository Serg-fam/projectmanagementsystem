package ua.goit.javaee.model;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
class NamedEntity extends BaseEntity {

  private String name;

  NamedEntity(Integer id, String name) {
    super(id);
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
