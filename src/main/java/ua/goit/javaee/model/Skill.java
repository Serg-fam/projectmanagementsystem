package ua.goit.javaee.model;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
public class Skill extends NamedEntity {

  public Skill(int id, String name) {
    super(id, name);
  }

  @Override
  public String toString() {
    return "Skill{" +
        "id=" + getId() +
        ", name='" + getName() + '\'' +
        '}';
  }

}
