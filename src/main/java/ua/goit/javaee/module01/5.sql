-- 5. Найти клиента (customer), которая приносит меньше всего прибыли компании (company) для каждой из компаний.

SELECT DISTINCT ON (company_name)
  companies.name AS company_name,
  projects.name  AS project_name,
  projects.cost  AS project_cost,
  customers.name AS customer_name
FROM companies
  INNER JOIN company_projects
    ON companies.id = company_projects.company_id
  INNER JOIN projects
    ON projects.id = company_projects.project_id
  INNER JOIN project_customers
    ON projects.id = project_customers.project_id
  INNER JOIN customers
    ON customers.id = customer_id
-- GROUP BY companies.name, projects.name, customers.name, projects.cost
ORDER BY
  company_name,
  project_cost ASC;
