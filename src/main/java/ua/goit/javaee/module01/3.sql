﻿-- 3. Вычислить общую ЗП всех Java разработчиков.

SELECT sum(d.salary)
FROM developer_skills ds
  INNER JOIN developers d
    ON ds.developer_id = d.id
  INNER JOIN skills s
    ON ds.skill_id = s.id
WHERE s.name = 'java';

/*
SELECT
  s.name,
  sum(d.salary)
FROM developer_skills ds
  INNER JOIN developers d
    ON ds.developer_id = d.id
  INNER JOIN skills s
    ON ds.skill_id = s.id
WHERE s.name = 'java'
GROUP BY s.name;
*/
