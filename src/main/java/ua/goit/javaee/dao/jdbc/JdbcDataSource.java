package ua.goit.javaee.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
abstract class JdbcDataSource {

  private static final Logger LOGGER = LoggerFactory.getLogger(JdbcDataSource.class);

  private static final String JDBC_DRIVER = JdbcSettings.JDBC_DRIVER_CLASS;
  private static final String DATABASE_URL = JdbcSettings.JDBC_URL;
  private static final String USER = JdbcSettings.JDBC_USER;
  private static final String PASSWORD = JdbcSettings.JDBC_PASSWORD;

  JdbcDataSource() {
    loadDriver();
  }

  private void loadDriver() {
    try {
      LOGGER.info("Loading JDBC driver: " + JDBC_DRIVER);
      Class.forName(JDBC_DRIVER);
      LOGGER.info("Driver loaded successfully");
    } catch (ClassNotFoundException e) {
      LOGGER.error("Can't find driver: " + JDBC_DRIVER, e);
      throw new RuntimeException(e);
    }
  }

  Connection getConnection() throws SQLException {
    return DriverManager.getConnection(DATABASE_URL, USER, PASSWORD);
  }

}
