package ua.goit.javaee.dao.jdbc;

import ua.goit.javaee.dao.CompanyDao;
import ua.goit.javaee.model.Developer;

import java.util.Set;

/**
 * Created by Стрела on 14.12.2016.
 */
public class JdbcCompanyDaoImpl extends JdbcDataSource implements CompanyDao {
    
    @Override
    public Developer findById(int id) {
        return null;
    }

    @Override
    public Set<Developer> getAll() {
        return null;
    }

    @Override
    public Developer save(Developer developer) {
        return null;
    }

    @Override
    public boolean delete(Developer developer) {
        return false;
    }
}
