package ua.goit.javaee.dao;

import java.util.Set;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
public interface AbstractDao<T> {

  T findById(int id);

  Set<T> getAll();

  T save(T t);

  boolean delete(T t);

}
