package ua.goit.javaee;

import ua.goit.javaee.dao.jdbc.JdbcDeveloperDaoImpl;
import ua.goit.javaee.model.Developer;
import ua.goit.javaee.model.Skill;

import java.util.HashSet;
import java.util.Set;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
public class Main {

  public static void main(String[] args) {

    JdbcDeveloperDaoImpl developerDao = JdbcDeveloperDaoImpl.getInstance();
    System.out.println(developerDao.findById(9));
    System.out.println("=====");
    System.out.println("TEST");

    Set<Skill> skills = new HashSet<>();
    skills.add(new Skill(1, "java"));
    skills.add(new Skill(3, "c#"));
  // skills.add(new Skill(5, "python"));
//    Developer developer = new Developer(17, "Roman", "Kopach", 32, 1100, skills);
    for (int i = 15; i < 250; i++) {
      developerDao.save(new Developer(i, "Roman1", "Kopach", 32, 1100, skills));
//      developerDao.delete(new Developer(i, "Roman", "Kopach", 32, 1100, skills));
    }
    Set<Developer> developers = developerDao.getAll();
    developers.forEach(System.out::println);
    System.out.println("=====");

  }

}
